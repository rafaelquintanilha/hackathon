const initialState = {
  name: '',
  email: '',
  age: '',
  password: '',
  university: '',
  team: '',
  isLogged: false
};

export default function reducer(state = initialState, action) {
	switch (action.type) {
		case 'SET_NAME':
			return Object.assign(
				{}, 
				state, 
				{ name: action.name }
			);

		case 'LOG_IN':
			return Object.assign(
				{}, 
				state, 
				{ isLogged: true }
			);

		case 'LOG_OUT':
			return Object.assign(
				{}, 
				state, 
				{ isLogged: false }
			);

		case 'CREATE_USER':
			return Object.assign(
				{},
				state,
				{ 
					name: action.name, 
					email: action.email,
					age: action.age,
					password: action.password,
					university: action.university,
					team: action.team
				}
			);

		default:
			return state;
	}
}
