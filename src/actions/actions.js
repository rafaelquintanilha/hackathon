export function setName(name) {
	return { type: 'SET_NAME', name };
}

export function createUser(args) {
	const { name, age, password, email, university, team } = args;
	return { 
		type: 'CREATE_USER', 
		name,
		age,
		password,
		email,
		university,
		team
	};
}

export function logIn() {
	return { type: 'LOG_IN' };
}

export function logOut() {
	return { type: 'LOG_OUT' };
}