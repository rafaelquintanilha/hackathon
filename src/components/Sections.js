import React from 'react';

export default class Sections extends React.Component {
	render() {
		return (
			<div className="row marketing">
				<div className="col-md-6">
					<h4>Subheading</h4>
					<p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

					<h4>Subheading</h4>
					<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>
				</div>

				<div className="col-md-6">
					<h4>Subheading</h4>
					<p>Donec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum.</p>

					<h4>Subheading</h4>
					<p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</p>			
				</div>
			</div>
		);
	}
}