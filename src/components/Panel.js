import React, { PropTypes } from 'react';

export default class Panel extends React.Component {
	render() {
		return (
			<div className="jumbotron">
				<h1>Fim</h1>
				<p className="lead">
					Obrigado, {this.props.state.name}
				</p>
				<p>A partir daqui, exibiremos dados do usuário e um campo
				para upload e remoção de arquivo.</p>
			</div>			
		);
	}
}

Panel.propTypes = {
	state: PropTypes.object
};