import React, { PropTypes } from 'react';
import { Link } from 'react-router';

export default class NavBar extends React.Component {

	static contextTypes = {
		router: PropTypes.object.isRequired
	}

	constructor(props) {
		super(props);
		this.URL = {
			home: "/hackathon/",
			about: "/hackathon/about",
			signup: "/hackathon/signup",
			signin: "/hackathon/signin",
			panel: "/hackathon/panel"
		};
	}

	setClass(URL) {
		return window.location.pathname === URL ? 'active' : '';
	}

	handleLogout() {
		this.props.logOut();
		this.context.router.push("/hackathon/");
	}

	signupOrControl() {
		const { signup, panel } = this.URL;
		return this.props.state.isLogged
			? <li className={this.setClass(panel)}><Link to={panel}>Painel de Controle</Link></li>
			: <li className={this.setClass(signup)}><Link to={signup}>Registrar</Link></li>;
	}

	loginOrLogout() {
		const { signin } = this.URL;
		return this.props.state.isLogged
			? <li><a href="#" onClick={this.handleLogout.bind(this)}>Sair</a></li>
			: <li className={this.setClass(signin)}><Link to={signin}>Entrar</Link></li>;
	}

	render() {
		const { home, about, signup, signin } = this.URL;
		return (
			<div>
				<div className="header clearfix">
					<nav>
						<ul className="nav nav-pills pull-right">
							<li className={this.setClass(home)}><Link to={home}>Início</Link></li>
							<li className={this.setClass(about)}><Link to={about}>Sobre</Link></li>
							{this.signupOrControl()}
							{this.loginOrLogout()}
						</ul>
					</nav>
					<h3 className="text-muted">Hackhaton</h3>
				</div>
			</div>		
		);
	}
}

NavBar.propTypes = {
	logOut: PropTypes.func,
	state: PropTypes.object
};