import React from 'react';
import Jumbotron from './Jumbotron.js';
import Sections from './Sections.js';

export default class HackathonContainer extends React.Component {
	render() {
		return (
			<div>
				<Jumbotron />
				<hr />
				<Sections />
			</div>		
		);
	}
}