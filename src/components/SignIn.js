import React, { PropTypes } from "react";

export default class SignIn extends React.Component {

	static contextTypes = {
		router: PropTypes.object.isRequired
	}

	constructor(props) {
		super(props);
		this.state = ({email: '', password: '', hasErrors: false});
	}

	handleClick() {
		const { email, password } = this.state;
		if ( email === this.props.state.email && password === this.props.state.password ) {
			this.setState({hasErrors: false});
			this.props.actions.logIn();
			this.context.router.push("/hackathon/panel");
		} else {
			this.setState({hasErrors: true});
		}
	}

	showLoginMessage() {
		return this.state.hasErrors 
			? <span className="form-error">E-mail ou senha inválidos.</span>
			: '';
	}

	render() {
		return (
			<div className="row">
				<div className="col-md-offset-3 col-md-6">
					<h3>Login</h3>
					{this.showLoginMessage()}
					<hr />
					<form>
						<div className="form-group">
							<input 
								type="email"
								onChange={(e) => this.setState({email: e.target.value})} 
								placeholder="Email"
								className="form-control" />
						</div>
						<div className="form-group">							
							<input 
								type="password"
								onChange={(e) => this.setState({password: e.target.value})} 
								placeholder="Senha"
								className="form-control" />
						</div>						
						<hr />
						<button 
							type="button"
							className="btn btn-success btn-block" 
							onClick={this.handleClick.bind(this)} >
							Entrar
						</button>						
					</form>
				</div>
			</div>
		);
	}
}

SignIn.propTypes = {
	actions: PropTypes.object,
	state: PropTypes.object
};