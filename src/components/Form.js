import React, { PropTypes } from "react";
import * as Constants from '../constants/Universities';

export default class Form extends React.Component {

	static contextTypes = {
		router: PropTypes.object.isRequired
	}

	constructor(props) {
		super(props);
		this.state = {
			hasErrors: true,
			email: '',
			password: '',
			confirmation: '',
			name: '',
			age: '',
			university: 'UFRJ',
			team: ''
		};
	}

	componentDidUpdate() {
		this.checkIfOk();
	}

	handleClick() {
		if ( this.checkIfOk() ) {
			this.props.actions.createUser(this.state);
			this.context.router.push("/hackathon/signin");
			window.scroll(0, 0); // Trick to go back to top
		}
	}

	checkIfOk() {
		if ( this.state.email !== '' 
			&& this.passwordIsValid()
			&& this.state.name !== '' 
			&& this.ageIsValid() 
			&& this.state.university !== '' 
			&& this.state.team !== '' 
		) {
			if ( this.state.hasErrors ) this.setState({hasErrors: false});
			return true;
		} else {
			if ( !this.state.hasErrors ) this.setState({hasErrors: true});
			return false;
		}
	}

	handleEmailChange(e) {
		const value = e.target.value;
		this.setState({email: value});
	}

	handleUniversityChange(e) {
		const value = e.target.value;
		this.setState({university: value});
	}

	handleTeamChange(e) {
		const value = e.target.value;
		this.setState({team: value});
	}

	handleNameChange(e) {
		const value = e.target.value;
		this.setState({name: value});
	}

	handlePasswordChange(e) {
		const value = e.target.value;
		this.setState({password: value});
	}

	handleConfirmationChange(e) {
		const value = e.target.value;
		this.setState({confirmation: value});
	}

	handleAgeChange(e) {
		const value = e.target.value;
		this.setState({age: value});
	}

	passwordIsValid() {
		return this.state.password === this.state.confirmation;
	}

	ageIsValid() {
		return this.state.age === '' || this.state.age >= 18;
	}

	showAgeMessage() {
		return ( !this.ageIsValid() ) 
			? <span className="form-error">Evento proibido para menores de 18 anos</span>
			: '';
	}

	showPasswordMessage() {
		return ( !this.passwordIsValid() ) 
			? <span className="form-error">Senhas não conferem</span>
			: '';
	}

	hasErrors() {
		if ( this.state.hasErrors ) return "disabled";
		return '';
	}

	showButtonMessage() {
		return ( this.state.hasErrors ) 
			? <span>Todos os campos são obrigatórios!</span>
			: '';	
	}

	render() {
		return (
			<div className="row">
				<div className="col-md-offset-3 col-md-6">
					<form>
						<div className="form-group">
							<label>Email</label>
							<input 
								type="email" 
								className="form-control" 
								value={this.state.email}
								onChange={this.handleEmailChange.bind(this)} />
						</div>
						<div className="form-group">
							<label>Senha</label>
							<input 
								type="password" 
								className="form-control" 
								value={this.state.password}
								onChange={this.handlePasswordChange.bind(this)} />
							{this.showPasswordMessage()}
						</div>
						<div className="form-group">
							<label>Confirme a Senha</label>
							<input 
								type="password" 
								className="form-control" 
								value={this.state.confirmation}
								onChange={this.handleConfirmationChange.bind(this)} />
							{this.showPasswordMessage()}
						</div>
						<hr />
						<div className="form-group">
							<label>Nome</label>
							<input 
								type="text" 
								className="form-control" 
								value={this.state.name}
								onChange={this.handleNameChange.bind(this)} />
						</div>
						<div className="form-group">
							<label>Idade</label>
							<input 
								type="number" 
								className="form-control"
								value={this.state.age}
								onChange={this.handleAgeChange.bind(this)} />
							{this.showAgeMessage()}
						</div>
						<div className="form-group">
							<label>Universidade</label>
							<select 
								className="form-control"
								value={this.state.university}
								onChange={this.handleUniversityChange.bind(this)} >
							{Constants.UNIVERSITIES.map( (u, i) => {
								return <option key={i} value={u}>{u}</option>;
							})}
							</select>
						</div>
						<div className="form-group">
							<label>Equipe</label>
							<input 
								type="text" 
								className="form-control" 
								value={this.state.team}
								onChange={this.handleTeamChange.bind(this)} />
						</div>
						<hr />
						<button 
							type="button"
							className="btn btn-success btn-block" 
							onClick={this.handleClick.bind(this)}
							disabled={this.hasErrors()}>
							Enviar
						</button>
						{this.showButtonMessage()}
					</form>
				</div>
			</div>
		);
	}
}

Form.propTypes = {
	actions: PropTypes.object
};