import React, { PropTypes } from 'react';
import { Link } from 'react-router';
import NavBar from './NavBar';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../actions/actions';

class App extends React.Component {
  render() {    
    const { children, actions, state } = this.props;
    return (
      <div className="container container-extended">
        <NavBar state={state} logOut={actions.logOut} />
        <br/>
        <div>
          {React.cloneElement(children, { actions: actions, state: state })}
        </div>
      </div>
    );  
  }
}

function mapStateToProps(state) {
  return {
    state: state.reducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  };
}

App.propTypes = {
  actions: PropTypes.object.isRequired,
  state: PropTypes.object.isRequired,
  children: PropTypes.element
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);