import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import AboutPage from './components/AboutPage.js';
import NotFoundPage from './components/NotFoundPage.js';
import HackathonContainer from './components/HackathonContainer.js';
import Form from './components/Form.js';
import SignIn from './components/SignIn.js';
import Panel from './components/Panel.js';

export default (
  <Route path="/hackathon/" component={App}>
    <IndexRoute component={HackathonContainer} />
    <Route path="signup" component={Form} />
    <Route path="signin" component={SignIn} />
    <Route path="panel" component={Panel} />
    <Route path="about" component={AboutPage}/>
    <Route path="*" component={NotFoundPage} />
  </Route>
);
